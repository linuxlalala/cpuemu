/*CPUEmu. Copyright (C) 2015, John Tzonevrakis, licensed under the GNU GPL version 2.0:*/
/*Copyright (C) 2015  John Tzonevrakis
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
/*
 * CPU description: 
 *
 * Registers:
 *  Our CPU has two registers, X and Y. Each of them is capable of holding
 *  an 8-bit value.
 * Address bus:
 *  Our address bus has a width of 16 bits.
 * Memory:
 *  Our refference machine has 1024 bytes of memory. You can tweak this by
 *  adjusting the lenght of memory[].
 * Instruction set:
 *  Our instruction set is quite simplistic. It currently consists of 12
 *  instructions, which I will present below in table form:
 *  |  Mnemonic | Opcode |                             Purpose                                |    Example    |
 *  |    NOP    |  0x00  |                            Do nothing.                             |      NOP      |
 *  |    LDX    |  0x01  |                         Load a value to X.                         |    LDX #$0    |
 *  |    LDY    |  0x02  |                         Load a value to Y.                         |    LDY #$0    |
 *  |    JMP    |  0x03  |                         Jump to an address.                        |    JMP $000   |
 *  |    STX    |  0x04  |                        Store X to an address.                      |    STX $000   |
 *  |    STY    |  0x05  |                        Store Y to an address.                      |    STY $000   |
 *  |    SMX    |  0x06  |                        Store an address to X.                      |    SMX $000   |
 *  |    SMY    |  0x07  |                        Store an address to Y.                      |    SMY $000   |
 *  |    BEX    |  0x08  |        Branch to memory location 2, if location 1 equals X.        | BEX $000,$010 |
 *  |    BNX    |  0x09  |   Branch to memory location 2, if it location 2 does not equal X.  | BNX $000,$010 |
 *  |    ADX    |  0x0A  |                        Add a number to X.                          |    ADX #$10   |
 *  |    ADY    |  0x0B  |                        Add a number to Y.                          |    ADY #$10   | 
 *  |    ORX    |  0x0C  |                        Bitwise OR with X.                          |    ORX #$10   |
 *  |    ORY    |  0x0D  |                        Bitwise OR with Y.                          |    ORY #$10   |
 *  |    ANX    |  0x0E  |                        Bitwise AND with X.                         |    ANX #$10   |
 *  |    ANY    |  0x0F  |                        Bitwise AND with Y.                         |    ANY #$10   |
 *  |    XOX    |  0x10  |                        Bitwise XOR with X.                         |    XOX #$10   |
 *  |    XOY    |  0x11  |                        Bitwise XOR with Y.                         |    XOY #$10   |
 *  |    NOX    |  0x12  |                          Inversion of X.                           |      NOX      |
 *  |    NOY    |  0x13  |                          Inversion of Y.                           |      NOY      |
 * TODO:
 *  [X] Implement math operators. :
 *   Done, but must be documented.
 *  [ ] Create an assembler.
 *  [ ] Read program from a file.
 *  [ ] Create some form of text I/O.
 * Legend:
 *   [X] -- Done.
 *   [P] -- Partially implemented.
 *   [N] -- Won't be implemented.
 *   [ ] -- Will (most probably) be implemented.
 */
FILE *fp;
uint8_t X;
uint8_t Y;
uint16_t PC;
uint8_t memory[1024];
int main();
void interpret(int val);
int main(int argc, char *argv[])
{
        uint16_t i;
        if(argc != 2) {
                fprintf(stderr, "Usage: cpu filename");
                exit(EXIT_FAILURE);
        } 
        /* Open the file which will be used to load memory contents. */
        fp = fopen(argv[1], "r");
        if(fp == NULL) {
                fprintf(stderr, "FATAL: Cannot open file for reading.");
                exit(EXIT_FAILURE);
        }
        for(i=0;i < 1024;i++) {
                memory[i] = fgetc(fp);
        }
        fclose(fp);
        /* "Walk through" the memory, interpreting whatever we find. */
	for(PC=0;PC < 65535;PC++) {
		interpret(memory[PC]);
                /* DEBUG: Show the states of X, Y and PC */
		printf(" X | Y | PC |\n %d %d %d\n", X,Y,PC); 
	}
	return 0;
}

void interpret(int val) {
        /*Interpret the opcode passed as parameter.*/
	switch (val) {
		case 0x00:
			/* Do nothing (NOP) */
			PC++;
			break;

		case 0x01:
			/* Load to X (LDX) */
			X = memory[PC+1];
			PC = PC+2;
			break;
		case 0x02:
			/* Load to Y (LDY) */
			Y = memory[PC+1];
			PC = PC+2;
			break;
		case 0x03:
			/* Go to a memory location (JMP) */
			PC = memory[PC+1];
                        PC = PC+2;
			break;
		case 0x04:
			/* Store X to specified memory location (STX) */
			memory[PC+1] = X;
			PC = PC+2;
			break;
		case 0x05:
			/* Store Y to specified memory location (STY) */
			memory[PC+1] = Y;
			PC = PC+2;
			break;
		case 0x06:
			/* Make X equal to a specified memory location (SMX) */
			X = memory[PC+1];
			PC = PC+2;
			break;
		case 0x07:
			/* Make Y equal to a specified memory location (SMY) */
			Y = memory[PC+1];
			PC = PC+2;
			break;
		case 0x08:
			/* Branch to a specific memory location, if the memory address given
                         * as parameter equals X. (BEX) */
			if(X == memory[memory[PC+1]]) {
				PC = memory[PC+2];
			}
			else {
				PC = PC+2;
			}
			break;
		case 0x09:
			/* Branch to a specific memory location, if the memory address
                         * given as parameter does *not* equal X. (BNX) */
			if(X != memory[memory[PC+1]]) {
				PC = memory[PC+2];
			}
			else {
				PC = PC+2;
			}
                        break;
                case 0x0A:
                        /* Add to X (ADX) */
                        X = X + memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x0B:
                        /* Add to Y (ADY) */
                        Y = Y + memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x0C:
                        /* Bitwise OR with X (ORX) */
                        X = X | memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x0D:
                        /* Bitwise OR with Y (ORY) */
                        Y = Y | memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x0E:
                        /* Bitwise AND with X (ANX) */
                        X = X & memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x0F:
                        /* Bitwise AND with Y (ANY) */
                        Y = Y & memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x10:
                        /* Bitwise XOR with X (XOX) */
                        X = X ^ memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x11:
                        /* Bitwise XOR with Y (XOY) */
                        Y = Y ^ memory[PC+1];
                        PC = PC+2;
                        break;
                case 0x12:
                        /* Inversion of X (NOX) */
                        X = ~X;
                        PC++;
                        break;
                case 0x13:
                        /* Inversion of Y (NOY) */
                        Y = ~Y;
                        PC++;
                        break;
        }
}
